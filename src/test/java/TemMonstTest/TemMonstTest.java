package TemMonstTest;

import Services.Utils;
import TemMonstPages.AccountPage;
import TemMonstPages.MainPage;
import Services.Wait;
import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.*;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public class TemMonstTest {
    private WebDriver driver;
    private Wait wait;
    private MainPage mainPage;
    private Utils utils;

    @BeforeClass
    void initTest() throws MalformedURLException {
        driver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"),
                                    DesiredCapabilities.chrome());
        driver.get("https://www.templatemonster.com/");
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
//        driver.manage().window().maximize();
        mainPage = PageFactory.initElements(driver, MainPage.class);
        wait = new Wait(driver, 10);
        utils = new Utils(driver);
    }

    @BeforeMethod
    void escClick(){
        driver.findElement(By.xpath(".//body")).sendKeys(Keys.ESCAPE);
        wait.waitPresenceOfElementByID("menu-favorites"); // "heart" icon
    }

    @Test
    void getCookieTestCase(){
        Cookie expectCookie = utils.getSomeCookie(driver, "aff");
        Assert.assertEquals(expectCookie.getValue(), "TM", "cookie value isn't expected");
    }

    @DataProvider
    Object[][] langs(){
        return new String[][]{
                            {"UA"},
                            {"FR"},
                            {"DE"}
        };
    }

    @Test(priority = 1, dataProvider = "langs")
    void languageLocaleTestCase(String language){
        mainPage.selectLanguage(language);
        Assert.assertTrue(utils.checkUrl(driver, language), "it isn't expected URL");
    }

    @Test(priority = 2)
    void signInTestCase(){
        AccountPage accountPage = PageFactory.initElements(driver, AccountPage.class);
        accountPage.signInWithFaceBook();
        Assert.assertTrue(accountPage.checkProfile(driver), "profile button isn't present");
    }

    @AfterClass()
    void afterTest(){
        driver.quit();
    }
}
