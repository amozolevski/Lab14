package TemMonstPages;

import Services.Wait;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class MainPage {
    private final WebDriver driver;
    private final Wait wait;

    public MainPage(WebDriver driver) {
        this.driver = driver;
        this.wait = new Wait(driver, 10);
    }

    @FindBy(xpath = "//*[contains(@class,'language-pick')]")
    private WebElement langMenu;

    public void selectLanguage(String language){
        String langID = "menu-" + language + "-locale";
        langMenu.click();
        wait.waitPresenceOfElementByID(langID);
        driver.findElement(By.id(langID)).click();
    }

}