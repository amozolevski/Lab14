package TemMonstPages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class FaceBookAccountPage {
    private WebDriver driver;

    public FaceBookAccountPage(WebDriver driver) {
        this.driver = driver;
    }

    @FindBy(id = "email")
    private WebElement email;

    @FindBy(id = "pass")
    private WebElement password;

    @FindBy(id = "loginbutton")
    private WebElement loginButton;

    public void signInFaceBook(String mail, String pass){
        email.sendKeys(mail);
        password.sendKeys(pass);
        loginButton.click();
    }
}
