package Services;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Wait {
    private WebDriverWait wait;

    public Wait(WebDriver driver, long s) {
        this.wait = new WebDriverWait(driver, s);
    }

    public void waitPresenceOfElementByID(String Id){
        wait.until((WebDriver a) -> a.findElement(By.id(Id)).isDisplayed());
    }

    public void waitChangeURL(String expectUrl) {
        wait.until((WebDriver b) -> b.getCurrentUrl().equals(expectUrl));
    }

    public void waitCookie(String cookieName){
        wait.until((WebDriver c) -> c.manage().getCookieNamed(cookieName));
    }

    public void waitChangeHandle(String handle){
        wait.until((WebDriver d) -> d.getWindowHandle().equals(handle));
    }

}
