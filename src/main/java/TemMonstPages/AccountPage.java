package TemMonstPages;

import Services.Wait;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.ArrayList;
import java.util.List;

public class AccountPage {
    private WebDriver driver;
    private Wait wait;

    public AccountPage(WebDriver driver) {
        this.driver = driver;
        this.wait = new Wait(driver, 10);
    }

    @FindBy(id = "header-signin-link")
    private WebElement accountButton;

    @FindBy(id = "id-general-facebook-button")
    private WebElement facebookButton;

    public void signInWithFaceBook(){
        String firstHandle = driver.getWindowHandle();
        accountButton.click();
        List<String> tabsList = new ArrayList<>(driver.getWindowHandles());
        driver.switchTo().window(tabsList.get(1));

        String secondHandle = driver.getWindowHandle();
        facebookButton.click();

        List<String> list = new ArrayList<>(driver.getWindowHandles());
        list.remove(firstHandle);
        list.remove(secondHandle);

        driver.switchTo().window(list.get(0));
        FaceBookAccountPage faceBookAccountPage = PageFactory.initElements(driver,FaceBookAccountPage.class);
        faceBookAccountPage.signInFaceBook("litstest@i.ua", "linux-32");

        driver.switchTo().window(secondHandle);
        wait.waitChangeHandle(secondHandle);
    }

    public boolean checkProfile(WebDriver driver){
        String expectElementID = "top-panel-to-profile-page-link";
        wait.waitPresenceOfElementByID(expectElementID);
        return driver.findElement(By.id(expectElementID)).isDisplayed();
    }

}