package Services;

import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriver;

public class Utils {
    private WebDriver driver;
    private Wait wait;

    public Utils(WebDriver driver) {
        this.driver = driver;
        this.wait = new Wait(driver, 10);
    }

    public Cookie getSomeCookie(WebDriver driver, String nameCookie) {
        wait.waitCookie(nameCookie);
        return driver.manage().getCookieNamed(nameCookie);
    }

    public boolean checkUrl(WebDriver driver, String language){
        String expectUrl = "https://www.templatemonster.com/" + language.toLowerCase() + "/";
        wait.waitChangeURL(expectUrl);
        return driver.getCurrentUrl().equals(expectUrl);
    }
}
